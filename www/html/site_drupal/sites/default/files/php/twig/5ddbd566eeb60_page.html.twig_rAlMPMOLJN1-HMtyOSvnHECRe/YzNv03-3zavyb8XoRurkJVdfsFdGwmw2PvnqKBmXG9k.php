<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* themes/space/templates/page.html.twig */
class __TwigTemplate_6c4c7d210b758a9cc287f0f2e8948094ec4dcb54c100d7987a905480f9ea62ca extends \Twig\Template
{
    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = [
        ];
        $this->sandbox = $this->env->getExtension('\Twig\Extension\SandboxExtension');
        $tags = ["if" => 67, "set" => 151];
        $filters = ["escape" => 70, "t" => 92];
        $functions = [];

        try {
            $this->sandbox->checkSecurity(
                ['if', 'set'],
                ['escape', 't'],
                []
            );
        } catch (SecurityError $e) {
            $e->setSourceContext($this->getSourceContext());

            if ($e instanceof SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        // line 67
        if (($context["is_front"] ?? null)) {
            // line 68
            echo "  <div id=\"header-top\" class=\"section\">
    ";
            // line 69
            if (($context["header_media_video"] ?? null)) {
                // line 70
                echo "      <video id=\"header-video\" class=\"ng-scope\" poster=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_video_still"] ?? null)), "html", null, true);
                echo "\" autoplay loop>
        <source src=\"";
                // line 71
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_media_url"] ?? null)), "html", null, true);
                echo "\" type=\"video/mp4\">
        Your browser does not support the video tag.
      </video>
    ";
            } else {
                // line 75
                echo "      <div id=\"header-image\" style=\"background: #000 url('";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["header_media_url"] ?? null)), "html", null, true);
                echo "') no-repeat fixed center bottom / cover \"></div>
    ";
            }
            // line 77
            echo "
    ";
            // line 78
            if (($context["overlay_styles"] ?? null)) {
                // line 79
                echo "      <div id=\"header-overlay\" style=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["overlay_styles"] ?? null)), "html", null, true);
                echo "\"></div>
    ";
            }
            // line 81
            echo "
    ";
            // line 82
            if (($context["screen"] ?? null)) {
                // line 83
                echo "      <div id=\"header-screen\"></div>
    ";
            }
            // line 85
            echo "    ";
            if (($context["fade"] ?? null)) {
                // line 86
                echo "      <div id=\"header-bg\"></div>
    ";
            }
            // line 88
            echo "    ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header_top", [])), "html", null, true);
            echo "

    <div class=\"section layout-container clearfix\">
      ";
            // line 91
            if (($context["logo"] ?? null)) {
                // line 92
                echo "        <a href=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["front_page"] ?? null)), "html", null, true);
                echo "\" title=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Home"));
                echo "\" rel=\"home\" id=\"logo\">
          <img src=\"";
                // line 93
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["space_logo"] ?? null)), "html", null, true);
                echo "\" alt=\"";
                echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Home"));
                echo "\" />
        </a>
      ";
            }
            // line 96
            echo "    </div>
    <div class=\"nav-down\"></div>
  </div>

  <div class=\"separator-wrapper\">
    <div class=\"separator separator-left\"></div>
    <div class=\"joint left\"></div>
    <div class=\"separator separator-middle\"></div>
    <div class=\"joint right\"></div>
    <div class=\"separator separator-right\"></div>
  </div>
";
        }
        // line 108
        echo "
<div id=\"page-wrapper\">
  <div id=\"page\">
    ";
        // line 111
        if ($this->getAttribute(($context["page"] ?? null), "main_menu", [])) {
            // line 112
            echo "      <div id=\"main-menu\">
        <!-- Show the site logo -->
        <img alt=\"";
            // line 114
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Home"));
            echo "\" src=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["space_logo"] ?? null)), "html", null, true);
            echo "\">
        ";
            // line 115
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "main_menu", [])), "html", null, true);
            echo "
      </div>
      <div class=\"menu-hamburger icon-menu\">
      </div>
    ";
        }
        // line 120
        echo "
    ";
        // line 121
        if ($this->getAttribute(($context["page"] ?? null), "header", [])) {
            // line 122
            echo "      <header id=\"header\" class=\"header\" role=\"banner\" aria-label=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Site header"));
            echo "\">
        ";
            // line 123
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "header", [])), "html", null, true);
            echo "
      </header>
    ";
        }
        // line 126
        echo "
    ";
        // line 127
        if ($this->getAttribute(($context["page"] ?? null), "featured", [])) {
            // line 128
            echo "      <div class=\"featured\" class=\"section\">
        <aside class=\"featured__inner section layout-container clearfix\" role=\"complementary\">
          ";
            // line 130
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "featured", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 134
        echo "
    ";
        // line 135
        if ($this->getAttribute(($context["page"] ?? null), "page_top", [])) {
            // line 136
            echo "      <div class=\"page-top\">
        <aside class=\"page-top__inner section layout-container clearfix\" role=\"complementary\">
          ";
            // line 138
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_top", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 142
        echo "
    <div id=\"main-wrapper\" class=\"layout-main-wrapper layout-container clearfix\" style=\"background: ";
        // line 143
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["region_content_image_url"] ?? null)), "html", null, true);
        echo "\">
      ";
        // line 144
        if (($context["region_content_screen"] ?? null)) {
            // line 145
            echo "        <div class=\"screen\"></div>
      ";
        }
        // line 147
        echo "      <div id=\"main\" class=\"layout-main clearfix\">
        ";
        // line 148
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "breadcrumb", [])), "html", null, true);
        echo "

        ";
        // line 150
        if (($this->getAttribute(($context["page"] ?? null), "sidebar_left", []) && $this->getAttribute(($context["page"] ?? null), "sidebar_right", []))) {
            // line 151
            echo "          ";
            $context["content_class"] = "content-both";
            // line 152
            echo "        ";
        } elseif (($this->getAttribute(($context["page"] ?? null), "sidebar_left", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_right", []))) {
            // line 153
            echo "          ";
            $context["content_class"] = "content-left";
            // line 154
            echo "        ";
        } elseif (($this->getAttribute(($context["page"] ?? null), "sidebar_right", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_left", []))) {
            // line 155
            echo "          ";
            $context["content_class"] = "content-right";
            // line 156
            echo "        ";
        } elseif (( !$this->getAttribute(($context["page"] ?? null), "sidebar_left", []) &&  !$this->getAttribute(($context["page"] ?? null), "sidebar_right", []))) {
            // line 157
            echo "          ";
            $context["content_class"] = "content-full";
            // line 158
            echo "        ";
        }
        // line 159
        echo "
        <main id=\"content\" class=\"column main-content ";
        // line 160
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["content_class"] ?? null)), "html", null, true);
        echo "\" role=\"main\">
          ";
        // line 161
        if ($this->getAttribute(($context["page"] ?? null), "content_top", [])) {
            // line 162
            echo "            <div class=\"content-top\">
              <aside class=\"content-top__inner section layout-container clearfix\" role=\"complementary\">
                ";
            // line 164
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_top", [])), "html", null, true);
            echo "
              </aside>
            </div>
          ";
        }
        // line 168
        echo "
          <section class=\"section\">
            <a id=\"main-content\" tabindex=\"-1\"></a>
            ";
        // line 171
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_prefix"] ?? null)), "html", null, true);
        echo "
            ";
        // line 172
        if (($context["title"] ?? null)) {
            // line 173
            echo "              <h1 class=\"title\" id=\"page-title\">
                ";
            // line 174
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title"] ?? null)), "html", null, true);
            echo "
              </h1>
            ";
        }
        // line 177
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["title_suffix"] ?? null)), "html", null, true);
        echo "
            ";
        // line 178
        if (($context["tabs"] ?? null)) {
            // line 179
            echo "              <nav class=\"tabs\" role=\"navigation\" aria-label=\"";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->renderVar(t("Tabs"));
            echo "\">
                ";
            // line 180
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["tabs"] ?? null)), "html", null, true);
            echo "
              </nav>
            ";
        }
        // line 183
        echo "            ";
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "help", [])), "html", null, true);
        echo "
            ";
        // line 184
        if (($context["action_links"] ?? null)) {
            // line 185
            echo "              <ul class=\"action-links\">";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["action_links"] ?? null)), "html", null, true);
            echo "</ul>
            ";
        }
        // line 187
        echo "
            ";
        // line 188
        echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content", [])), "html", null, true);
        echo "
          </section>

          ";
        // line 191
        if ($this->getAttribute(($context["page"] ?? null), "content_bottom", [])) {
            // line 192
            echo "            <div class=\"content-bottom\">
              <aside class=\"content-bottom__inner section layout-container clearfix\" role=\"complementary\">
                ";
            // line 194
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_bottom", [])), "html", null, true);
            echo "
              </aside>
            </div>
          ";
        }
        // line 198
        echo "
        </main>
        ";
        // line 200
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_left", [])) {
            // line 201
            echo "          <div id=\"sidebar-left\" class=\"column sidebar\">
            <aside class=\"section\" role=\"complementary\">
              ";
            // line 203
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_left", [])), "html", null, true);
            echo "
            </aside>
          </div>
        ";
        }
        // line 207
        echo "        ";
        if ($this->getAttribute(($context["page"] ?? null), "sidebar_right", [])) {
            // line 208
            echo "          <div id=\"sidebar-right\" class=\"column sidebar\">
            <aside class=\"section\" role=\"complementary\">
              ";
            // line 210
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "sidebar_right", [])), "html", null, true);
            echo "
            </aside>
          </div>
        ";
        }
        // line 214
        echo "      </div>
    </div>

    <div class=\"separator-footer\"></div>

    ";
        // line 219
        if ($this->getAttribute(($context["page"] ?? null), "content_2", [])) {
            // line 220
            echo "      <div class=\"content_2_wrapper\" style=\"background: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["region_content_2_image_url"] ?? null)), "html", null, true);
            echo "\">
        ";
            // line 221
            if (($context["region_content_2_screen"] ?? null)) {
                // line 222
                echo "          <div class=\"screen\"></div>
        ";
            }
            // line 224
            echo "        <div class=\"content_2\">
          <aside class=\"content_2__inner section layout-container clearfix\" role=\"complementary\">
            ";
            // line 226
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_2", [])), "html", null, true);
            echo "
          </aside>
        </div>
      </div>
    ";
        }
        // line 231
        echo "
    ";
        // line 232
        if ($this->getAttribute(($context["page"] ?? null), "content_3", [])) {
            // line 233
            echo "      <div class=\"content_3_wrapper\" style=\"background: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["region_content_3_image_url"] ?? null)), "html", null, true);
            echo "\">
        ";
            // line 234
            if (($context["region_content_3_screen"] ?? null)) {
                // line 235
                echo "          <div class=\"screen\"></div>
        ";
            }
            // line 237
            echo "        <div class=\"content_3\">
          <aside class=\"content_3__inner section layout-container clearfix\" role=\"complementary\">
            ";
            // line 239
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_3", [])), "html", null, true);
            echo "
          </aside>
        </div>
      </div>
    ";
        }
        // line 244
        echo "
    ";
        // line 245
        if ($this->getAttribute(($context["page"] ?? null), "content_4", [])) {
            // line 246
            echo "      <div class=\"content_4_wrapper\" style=\"background: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["region_content_4_image_url"] ?? null)), "html", null, true);
            echo "\">
        ";
            // line 247
            if (($context["region_content_4_screen"] ?? null)) {
                // line 248
                echo "          <div class=\"screen\"></div>
        ";
            }
            // line 250
            echo "        <div class=\"content_4\">
          <aside class=\"content_4__inner section layout-container clearfix\" role=\"complementary\">
            ";
            // line 252
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_4", [])), "html", null, true);
            echo "
          </aside>
        </div>
      </div>
    ";
        }
        // line 257
        echo "
    ";
        // line 258
        if ($this->getAttribute(($context["page"] ?? null), "content_5", [])) {
            // line 259
            echo "      <div class=\"content_5_wrapper\" style=\"background: ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["region_content_5_image_url"] ?? null)), "html", null, true);
            echo "\">
        ";
            // line 260
            if (($context["region_content_5_screen"] ?? null)) {
                // line 261
                echo "          <div class=\"screen\"></div>
        ";
            }
            // line 263
            echo "        <div class=\"content_5\">
          <aside class=\"content_5__inner section layout-container clearfix\" role=\"complementary\">
          ";
            // line 265
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "content_5", [])), "html", null, true);
            echo "
          </aside>
        </div>
      </div>
    ";
        }
        // line 270
        echo "
    ";
        // line 271
        if ($this->getAttribute(($context["page"] ?? null), "page_bottom", [])) {
            // line 272
            echo "      <div class=\"page-bottom\">
        <aside class=\"page-bottom__inner section layout-container clearfix\" role=\"complementary\">
          ";
            // line 274
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "page_bottom", [])), "html", null, true);
            echo "
        </aside>
      </div>
    ";
        }
        // line 278
        echo "
    <footer class=\"site-footer\">
      ";
        // line 280
        if (($context["social_icons"] ?? null)) {
            // line 281
            echo "        ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed(($context["social_icons"] ?? null)), "html", null, true);
            echo "
      ";
        }
        // line 283
        echo "      ";
        if ((($this->getAttribute(($context["page"] ?? null), "footer_left", []) || $this->getAttribute(($context["page"] ?? null), "footer_middle", [])) || $this->getAttribute(($context["page"] ?? null), "footer_right", []))) {
            // line 284
            echo "        <div class=\"site-footer__top layout-container clearfix\">
          ";
            // line 285
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_left", [])), "html", null, true);
            echo "
          ";
            // line 286
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_middle", [])), "html", null, true);
            echo "
          ";
            // line 287
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_right", [])), "html", null, true);
            echo "
        </div>
      ";
        }
        // line 290
        echo "      ";
        if ($this->getAttribute(($context["page"] ?? null), "footer_bottom", [])) {
            // line 291
            echo "        ";
            echo $this->env->getExtension('Drupal\Core\Template\TwigExtension')->escapeFilter($this->env, $this->sandbox->ensureToStringAllowed($this->getAttribute(($context["page"] ?? null), "footer_bottom", [])), "html", null, true);
            echo "
      ";
        }
        // line 293
        echo "    </footer>
  </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "themes/space/templates/page.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  562 => 293,  556 => 291,  553 => 290,  547 => 287,  543 => 286,  539 => 285,  536 => 284,  533 => 283,  527 => 281,  525 => 280,  521 => 278,  514 => 274,  510 => 272,  508 => 271,  505 => 270,  497 => 265,  493 => 263,  489 => 261,  487 => 260,  482 => 259,  480 => 258,  477 => 257,  469 => 252,  465 => 250,  461 => 248,  459 => 247,  454 => 246,  452 => 245,  449 => 244,  441 => 239,  437 => 237,  433 => 235,  431 => 234,  426 => 233,  424 => 232,  421 => 231,  413 => 226,  409 => 224,  405 => 222,  403 => 221,  398 => 220,  396 => 219,  389 => 214,  382 => 210,  378 => 208,  375 => 207,  368 => 203,  364 => 201,  362 => 200,  358 => 198,  351 => 194,  347 => 192,  345 => 191,  339 => 188,  336 => 187,  330 => 185,  328 => 184,  323 => 183,  317 => 180,  312 => 179,  310 => 178,  305 => 177,  299 => 174,  296 => 173,  294 => 172,  290 => 171,  285 => 168,  278 => 164,  274 => 162,  272 => 161,  268 => 160,  265 => 159,  262 => 158,  259 => 157,  256 => 156,  253 => 155,  250 => 154,  247 => 153,  244 => 152,  241 => 151,  239 => 150,  234 => 148,  231 => 147,  227 => 145,  225 => 144,  221 => 143,  218 => 142,  211 => 138,  207 => 136,  205 => 135,  202 => 134,  195 => 130,  191 => 128,  189 => 127,  186 => 126,  180 => 123,  175 => 122,  173 => 121,  170 => 120,  162 => 115,  156 => 114,  152 => 112,  150 => 111,  145 => 108,  131 => 96,  123 => 93,  116 => 92,  114 => 91,  107 => 88,  103 => 86,  100 => 85,  96 => 83,  94 => 82,  91 => 81,  85 => 79,  83 => 78,  80 => 77,  74 => 75,  67 => 71,  62 => 70,  60 => 69,  57 => 68,  55 => 67,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Source("", "themes/space/templates/page.html.twig", "/var/www/html/html/site_drupal/themes/space/templates/page.html.twig");
    }
}
